package com.curiouslabs.authenticator.repository

import javax.inject.Inject

import com.curiouslabs.authenticator.model.User
import com.curiouslabs.authenticator.repository.BSONFormats._
import com.curiouslabs.mongo.{CrudRepository}
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by visitor15 on 11/8/16.
  */
class UserRepository @Inject() (authenticatorDBConfiguration: AuthenticatorDBConfiguration) extends CrudRepository[PersistedUser] {
  implicit val userCollection = authenticatorDBConfiguration.getMongoDataStore("user").collection

  private def readOneInternal(bSONDocument: BSONDocument) = readOne { bSONDocument }.map { optPersistedUser => optPersistedUser.map { p => p.toUser } }

  def saveUser(user: User) = save(PersistedUser.fromUser(user)) { BSONDocument("_id" -> user.id) }
  def findUserById(id: String) = readOneInternal(BSONDocument("_id" -> id))
  def findUserByEmail(email: String) = readOneInternal(BSONDocument("email" -> email))
}

case class PersistedUser(_id: String,
                         creationDate:  Long,
                         lastUpdated:   Long,
                         firstName:     String,
                         lastName:      String,
                         email:         String,
                         displayName:   Option[String]) {
  def toUser: User = User(_id, creationDate, lastUpdated, firstName, lastName, email, displayName)
}

object PersistedUser {
  def fromUser(user: User) = PersistedUser(user.id, user.creationDate, user.lastUpdated, user.firstName, user.lastName, user.email, user.displayName)
}
