package com.curiouslabs.authenticator.repository

import java.util.Date
import javax.inject.Inject

import com.curiouslabs.authenticator.model.AuthenticationReceipt
import com.curiouslabs.authenticator.repository.BSONFormats._
import com.curiouslabs.mongo.CrudRepository
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by visitor15 on 11/20/16.
  */
class AuthRepository @Inject() (authenticatorDBConfiguration: AuthenticatorDBConfiguration) extends CrudRepository[PersistedAuthTicket] {
  implicit val authCollection = authenticatorDBConfiguration.getMongoDataStore("auth").collection

  def findAuthenticationById(id: String) = readOne { BSONDocument("_id" -> id) }.map { optAuth => optAuth.map { p => p.toAuthenticationReceipt }}
  def findAuthenticationByToken(token: String) = readOne { BSONDocument("accessToken" -> token) }.map { optAuth => optAuth.map { p => p.toAuthenticationReceipt }}
  def saveAuthentication(authenticationReceipt: AuthenticationReceipt) = save(PersistedAuthTicket.toPersistedAuthTicket(authenticationReceipt)) { BSONDocument("_id" -> authenticationReceipt.userId) }
}

case class PersistedAuthTicket(_id:           String,
                               accessToken:   String,
                               refreshToken:  String,
                               tokenType:     String,
                               expiresIn:     Long,
                               creationDate:  Date = new Date()) {
  def toAuthenticationReceipt: AuthenticationReceipt = AuthenticationReceipt(_id, accessToken, tokenType, expiresIn, Some(creationDate.getTime), refreshToken)
}

object PersistedAuthTicket {
  def toPersistedAuthTicket(authenticationReceipt: AuthenticationReceipt) = {
    PersistedAuthTicket(authenticationReceipt.userId, authenticationReceipt.accessToken, authenticationReceipt.refreshToken, authenticationReceipt.tokenType, authenticationReceipt.expiresIn)
  }
}