package com.curiouslabs.authenticator.repository

import java.util.{Base64, UUID}
import javax.inject.Inject

import com.curiouslabs.authenticator.repository.BSONFormats._
import com.curiouslabs.authenticator.service.UserCreationCommand
import com.curiouslabs.mongo.CrudRepository
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by visitor15 on 11/20/16.
  */
class UserCredentialRepository @Inject() (authenticatorDBConfiguration: AuthenticatorDBConfiguration) extends CrudRepository[PersistedUserCredentials] {
  implicit val credentialsCollection = authenticatorDBConfiguration.getMongoDataStore("credential").collection

  def findCredentialsById(id: String) = readOne { BSONDocument("_id" -> id) }.map { optCredentials => optCredentials.map { p => p.toUserCredentials }}
  def findCredentialsByEmail(email: String) = readOne { BSONDocument("email" -> email) }.map { optCredentials => optCredentials.map { p => p.toUserCredentials }}
  def saveCredentials(userCredentials: UserCredentials) = save(PersistedUserCredentials.toPersistedUserCredentials(userCredentials)) { BSONDocument("_id" -> userCredentials.id) }
}

case class UserCredentials(id:        String,
                           email:     String,
                           password:  String)

object UserCredentials {
  def generateNewCredentials(userCreationCommand: UserCreationCommand) =
    UserCredentials(
      UUID.randomUUID().toString,
      userCreationCommand.email,
      userCreationCommand.password)

  def emptyCredentials = UserCredentials("", "", "")
}

case class PersistedUserCredentials(_id:      String,
                                    email:    String,
                                    password: String) {
  def toUserCredentials: UserCredentials = UserCredentials(_id, email, new String(Base64.getUrlDecoder.decode(password)))
}

object PersistedUserCredentials {
  def toPersistedUserCredentials(userCredentials: UserCredentials): PersistedUserCredentials =
    PersistedUserCredentials(userCredentials.id,
    userCredentials.email,
    new String(Base64.getUrlEncoder.encode(userCredentials.password.getBytes)))
}
