package com.curiouslabs.authenticator.repository

import com.curiouslabs.authenticator.model.User
import reactivemongo.bson.Macros

/**
  * Created by visitor15 on 11/8/16.
  */
object BSONFormats {
  implicit val User_MacroHandler                      = Macros.handler[User]
  implicit val PersistedUser_MacroHandler             = Macros.handler[PersistedUser]
  implicit val PersistedAuthTicket_MacroHandler       = Macros.handler[PersistedAuthTicket]
  implicit val PersistedUserCredentials_MacroHandler  = Macros.handler[PersistedUserCredentials]
}