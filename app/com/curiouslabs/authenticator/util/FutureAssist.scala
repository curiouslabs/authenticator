package com.curiouslabs.authenticator.util

import scala.concurrent.Future

/**
  * Created by visitor15 on 1/31/17.
  */
trait FutureAssist {
  def asFuture[T](f: => T) = Future.successful(f)
}
