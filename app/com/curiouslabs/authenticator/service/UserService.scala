package com.curiouslabs.authenticator.service

import javax.inject.Inject

import com.curiouslabs.authenticator.error.UserServiceError
import com.curiouslabs.authenticator.model.User
import com.curiouslabs.authenticator.repository.UserRepository

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by visitor15 on 11/7/16.
  */
class UserService @Inject()(userRepository: UserRepository)(implicit ec: ExecutionContext) {
  def create(createRequest: UserCreationCommand): Future[Either[UserServiceError, User]] = {
    val user = User.fromCreationCommand(createRequest)
    userRepository.saveUser(user).map { writeResult =>
      if(writeResult.ok) Right(user) else Left(UserServiceError(500, "Unable to create user"))
    }
  }

  def read(readRequest: UserReadCommand): Future[Either[UserServiceError, User]] = {
    userRepository.findUserById(readRequest.id).map { optUser =>
      optUser match {
        case Some(u)  => Right(u)
        case None     => Left(UserServiceError(404, s"User ${readRequest.id} not found"))
      }
    }
  }

  def update(updateRequest: UserUpdateCommand): Future[Either[UserServiceError, User]] = {
    Future.successful(Left(UserServiceError(500, "Unsupported action")))
  }

  def delete(deleteRequest: UserDeleteCommand): Future[Either[UserServiceError, User]] = {
    Future.successful(Left(UserServiceError(500, "Unsupported action")))
  }

  def update(user: User): Future[Either[UserServiceError, User]] = {
    userRepository.saveUser(user).map { writeResult =>
      if (writeResult.ok) Right(user) else Left(UserServiceError(500, s"Error updating user ${user.id}"))
    }
  }
}