package com.curiouslabs.authenticator.service

import java.util.Base64
import javax.inject.Inject

import com.curiouslabs.authenticator.model.{AuthenticationReceipt, User}
import com.curiouslabs.authenticator.repository.{AuthRepository, UserCredentialRepository, UserCredentials}
import com.curiouslabs.authenticator.util.FutureAssist

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by visitor15 on 11/20/16.
  */
class AuthenticationService @Inject() (userService: UserService,
                                       authRepository: AuthRepository,
                                       credentialRepository: UserCredentialRepository)(implicit ec: ExecutionContext) extends FutureAssist {

  def isLoggedIn(id: String): Future[Boolean] = {
    for {
      userE           <- userService.read(UserReadCommand(id))
      optAuthReceipt  <- userE match {
        case Left(e)  => asFuture(None)
        case Right(u) => authRepository.findAuthenticationById(u.id)
      }
      isLoggedIn      <- asFuture(isValid(optAuthReceipt.getOrElse(AuthenticationReceipt.emptyReceipt)))
    } yield isLoggedIn
  }

  def createNewUserAuthentication(user: User): Future[Option[AuthenticationReceipt]] = {
    val authentication = createAuthenticationReceipt(user)
    authRepository.saveAuthentication(authentication).map { writeResult =>
      if(writeResult.ok) Some(authentication) else None
    }
  }

  def isValidCredentials(usernamePassword: UsernamePassword): Future[Boolean] = {
    credentialRepository.findCredentialsByEmail(usernamePassword.username).map { optCredentials =>
      optCredentials match {
        case Some(c)  => c.email == usernamePassword.username && c.password == usernamePassword.password
        case None     => false
      }
    }
  }

  def getCredentialsByUsername(username: String): Future[Option[UserCredentials]] = {
    credentialRepository.findCredentialsByEmail(username)
  }

  def getAuthenticationReceiptByAccessToken(accessToken: String): Future[Option[AuthenticationReceipt]] = {
    authRepository.findAuthenticationByToken(accessToken)
  }

  // TODO - Implement properly
  private def createAuthenticationReceipt(user: User): AuthenticationReceipt = {
    val accessToken   = Base64.getUrlEncoder.encodeToString(new String(user.id + user.creationDate + user.email + System.currentTimeMillis).getBytes)
    val refreshToken  = Base64.getUrlEncoder.encodeToString(new String(user.id + user.creationDate + user.email + accessToken).getBytes())
    AuthenticationReceipt(user.id, accessToken, "bearer", 1800, None, refreshToken)
  }

  def isValid(authTicket: AuthenticationReceipt): Boolean = {
    (authTicket.creationDate.get + (authTicket.expiresIn * 1000)) > System.currentTimeMillis()
  }
}
