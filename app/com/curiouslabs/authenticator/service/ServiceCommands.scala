package com.curiouslabs.authenticator.service

/**
  * Created by nchampagne on 1/30/17.
  */
trait CreateCommand
trait ReadCommand
trait UpdateCommand
trait DeleteCommand

case class UserCreationCommand(email:       String,
                               password:    String,
                               firstName:   String,
                               lastName:    String,
                               displayName: Option[String],
                               id:          Option[String]) extends CreateCommand

case class UserReadCommand(id: String) extends ReadCommand

case class UserUpdateCommand(id: String,
                             email: Option[String],
                             firstName: Option[String],
                             lastName: Option[String],
                             displayName: Option[String]) extends UpdateCommand

case class UserDeleteCommand(id: String) extends DeleteCommand
