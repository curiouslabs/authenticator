package com.curiouslabs.authenticator.service

import javax.inject.Inject

import com.curiouslabs.authenticator.error.RegistrationServiceError
import com.curiouslabs.authenticator.repository.{UserCredentialRepository, UserCredentials}
import com.curiouslabs.authenticator.util.FutureAssist

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by visitor15 on 11/20/16.
  */
class RegistrationService @Inject() (userCredentialRepository: UserCredentialRepository)(implicit ec: ExecutionContext) extends FutureAssist {
  def createNewRegistrant(userCreationRequest: UserCreationCommand): Future[Either[RegistrationServiceError, UserCredentials]] = {
    for {
      userCredentials <- userCredentialRepository.findCredentialsByEmail(userCreationRequest.email)
      result          <- registrationResultFromCredentialQuery(userCreationRequest, userCredentials)
    } yield result
  }

  private def registrationResultFromCredentialQuery(userCreationRequest: UserCreationCommand, optUserCredentials: Option[UserCredentials]): Future[Either[RegistrationServiceError, UserCredentials]] = {
    optUserCredentials match {
      case Some(c)  => asFuture(Left(RegistrationServiceError(412, s"User ${userCreationRequest.email} already exists")))
      case None     => {
        val credentials = UserCredentials.generateNewCredentials(userCreationRequest);
        userCredentialRepository.saveCredentials(credentials).map { writeResult =>
          if (writeResult.ok) Right(credentials) else Left(RegistrationServiceError(500, s"Something went wrong creating ${userCreationRequest.email}"))
        }
      }
    }
  }
}


