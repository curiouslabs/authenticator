package com.curiouslabs.authenticator.service

import java.util.Base64
import javax.inject.Inject

import com.curiouslabs.authenticator.model.{AuthenticatedRequest, AuthenticationReceipt, BasicAuthenticatedRequest}
import com.curiouslabs.authenticator.util.FutureAssist
import play.api.mvc.{Action, Headers, Result, Results}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

/**
  * Created by visitor15 on 11/20/16.
  */
class AuthenticationActionService @Inject()(userService: UserService,
                                            authenticationService: AuthenticationService) extends Results with FutureAssist {

  def Authenticated(f: AuthenticatedRequest => Future[Result]) = {
    Action.async { request =>
      BearerAuthHelper.retrieveAccessToken(request.headers).map { accessToken =>
        for {
          optAuthenticationReceipt  <-  authenticationService.getAuthenticationReceiptByAccessToken(accessToken)
          authenticationReceipt     =   optAuthenticationReceipt.getOrElse(AuthenticationReceipt.emptyReceipt)
          isValid                   <-  asFuture(authenticationService.isValid(authenticationReceipt))
          userResult                <-  userService.read(UserReadCommand(authenticationReceipt.userId))
          response                  <-  if(isValid) {
                                          userResult match {
                                            case Left(e)  => asFuture(Unauthorized(e.message))
                                            case Right(u) => f(AuthenticatedRequest(u, request))
                                          }
                                        }
                                        else asFuture(Unauthorized("Invalid authorization"))
        } yield response
      }.getOrElse(asFuture(Unauthorized("No Authentication header found")))
    }
  }

  def BasicAuth(f: BasicAuthenticatedRequest => Future[Result]) = {
    Action.async { request =>
      BasicAuthHelper.retrieveBasicAuth(request.headers).map { basicAuthToken =>
        BasicAuthHelper.decode(basicAuthToken).map { basicAuth =>
          for {
            isValid   <-  authenticationService.isValidCredentials(basicAuth)
            response  <-  if (isValid) f(BasicAuthenticatedRequest(basicAuth, request)) else asFuture(Unauthorized("Invalid credentials"))
          } yield response
        }.getOrElse(asFuture(Unauthorized("Invalid authentication")))
      }.getOrElse(asFuture(Unauthorized("No Authentication header found")))
    }
  }
}

object BasicAuthHelper {
  def retrieveBasicAuth(headers: Headers): Option[String] = {
    headers.get("Authorization").flatMap { str => if (str.contains("Basic")) Option((str.split(" ").apply(1))) else None }
  }

  def decodeFromHeader(headers: Headers): Option[UsernamePassword] = {
    headers.get("Authorization").flatMap { str => decode(str.split(" ").apply(1)) }
  }

  def decode(encodedStr: String): Option[UsernamePassword] = {
    Try {
      val decodedStr  = new String(Base64.getUrlDecoder.decode(encodedStr))
      val strArray    = decodedStr.split(":", decodedStr.lastIndexOf(":"))
      UsernamePassword(strArray.apply(0), strArray.apply(1))
    } match {
      case Success(v) => Some(v)
      case Failure(e) => None
    }
  }
}

object BearerAuthHelper {
  def retrieveAccessToken(headers: Headers): Option[String] = {
    headers.get("Authorization").flatMap { str => if (str.contains("Bearer")) Option((str.split(" ").apply(1))) else None }
  }
}

case class UsernamePassword(username: String, password: String, id: Option[String] = None)