package com.curiouslabs.authenticator.error

/**
  * Created by visitor15 on 11/7/16.
  */
trait ServiceError {
  def code: Int
  def message: String
}

trait RepositoryError {
  def code: Int
  def message: String
}

case class UserServiceError(code: Int, message: String) extends ServiceError
case class RegistrationServiceError(code: Int, message: String) extends ServiceError

case class UserRepositoryError(code: Int, message: String) extends RepositoryError