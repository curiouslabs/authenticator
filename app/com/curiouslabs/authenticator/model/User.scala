package com.curiouslabs.authenticator.model

import com.curiouslabs.authenticator.service.UserCreationCommand

/**
  * Created by visitor15 on 11/7/16.
  */
case class User(id:           String,
                creationDate: Long,
                lastUpdated:  Long,
                firstName:    String,
                lastName:     String,
                email:        String,
                displayName:  Option[String]) {
}

object User {
  def fromCreationCommand(userCreationRequest: UserCreationCommand) =
    User(userCreationRequest.id.get,
      System.currentTimeMillis,
      System.currentTimeMillis,
      userCreationRequest.firstName,
      userCreationRequest.lastName,
      userCreationRequest.email,
      userCreationRequest.displayName)
}