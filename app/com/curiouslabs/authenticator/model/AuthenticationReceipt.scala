package com.curiouslabs.authenticator.model

/**
  * Created by visitor15 on 11/20/16.
  */
case class AuthenticationReceipt(userId:        String,
                                 accessToken:   String,
                                 tokenType:     String,
                                 expiresIn:     Long,
                                 creationDate:  Option[Long],
                                 refreshToken:  String)

object AuthenticationReceipt {
  def emptyReceipt = AuthenticationReceipt("", "", "", 0, Some(System.currentTimeMillis()), "")
}