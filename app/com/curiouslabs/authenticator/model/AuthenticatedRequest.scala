package com.curiouslabs.authenticator.model

import com.curiouslabs.authenticator.service.UsernamePassword
import play.api.mvc.{AnyContent, Request, WrappedRequest}

/**
  * Created by visitor15 on 11/19/16.
  */
case class AuthenticatedRequest(user: User, private val request: Request[AnyContent]) extends WrappedRequest(request)

case class BasicAuthenticatedRequest(usernamePassword: UsernamePassword, private val request: Request[AnyContent]) extends WrappedRequest(request)

